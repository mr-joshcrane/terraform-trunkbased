#!/bin/bash

## Install the CLI utility
apt-get update && apt-get install -y software-properties-common
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
apt-get update && apt-get install terraform

## Configure the CLI credentials
cat << EOF > .terraformrc
credentials "app.terraform.io" {
  token = "${TERRAFORM_CLI_TOKEN}"
}

EOF

export TF_CLI_CONFIG_FILE="../../.terraformrc"