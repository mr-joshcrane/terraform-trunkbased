terraform {
  cloud {
    organization = "joshcrane-test"

    workspaces {
      name = "dev"
    }
  }
}

terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.27"
        }
    }
}

provider "aws" {
    profile = "default"
    region = "ap-southeast-2"
}

resource "aws_s3_bucket" "test_bucket" {
    bucket = "terraform-test-1111-${var.environment}"
    acl = "private"

    tags = {
        Environment = var.environment
    }

}