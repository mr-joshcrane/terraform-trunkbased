variable "environment" {
  description = "name of the environment"
  type        = string
  default     = "prod"
}
